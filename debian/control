Source: owncloud-client-desktop-shell-integration-nautilus
Section: net
Priority: optional
Maintainer: ownCloud for Debian maintainers <pkg-owncloud-maintainers@lists.alioth.debian.org>
Uploaders: Pierre-Elliott Bécue <peb@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               dh-python,
               extra-cmake-modules,
               python3-all,
Vcs-Git: https://salsa.debian.org/owncloud-team/owncloud-client-desktop-shell-integration-nautilus.git
Vcs-Browser: https://salsa.debian.org/owncloud-team/owncloud-client-desktop-shell-integration-nautilus
Standards-Version: 4.6.2
Homepage: https://github.com/owncloud/client-desktop-shell-integration-nautilus

Package: nautilus-owncloud
Architecture: all
Section: gnome
Depends: nautilus,
         owncloud-client,
         owncloud-client-data,
         python3-nautilus,
         ${misc:Depends},
         ${python3:Depends}
Suggests: nautilus-script-manager
Enhances: nautilus
Description: ownCloud integration for Nautilus
 The ownCloudSync system lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Nautilus ownCloud is an extension that integrates the ownCloud web service with
 your GNOME Desktop.

Package: nemo-owncloud
Architecture: all
Depends: nemo,
         nemo-python,
         owncloud-client,
         owncloud-client-data,
         ${misc:Depends},
         ${python3:Depends}
Enhances: nemo
Description: ownCloud integration for Nemo
 The ownCloudSync system lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Nemo ownCloud is an extension that integrates the ownCloud web service with
 your Cinnamon Desktop.

Package: caja-owncloud
Architecture: all
Depends: caja,
         owncloud-client,
         owncloud-client-data,
         python3-caja,
         python3-gi,
         ${misc:Depends},
         ${python3:Depends}
Enhances: caja
Description: ownCloud integration for Caja
 The ownCloudSync system lets you always have your latest files wherever
 you are. Just specify one or more folders on the local machine to and a server
 to synchronize to. You can configure more computers to synchronize to the same
 server and any change to the files on one computer will silently and reliably
 flow across to every other.
 .
 Nemo ownCloud is an extension that integrates the ownCloud web service with
 your MATE Desktop.
